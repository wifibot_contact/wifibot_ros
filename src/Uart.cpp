//
// Created by rbeal on 15/03/17.
//

#include "Uart.h"
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <fcntl.h>
#include <string.h>
#include "ros/ros.h"
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>

Uart::Uart() {

}

Uart::~Uart() {
    
    close(this->m_fd);
}

Uart::Uart(string path, int speed) {
    
    struct termios options;
    (void) speed; // TODO

    this->m_fd = open(path.c_str(), O_RDWR | O_NOCTTY);
    if (this->m_fd <= 0) {
        ROS_ERROR("UNABLE TO OPEN UART");
        return;
    }

    tcgetattr(this->m_fd, &options);

    cfsetispeed(&options, B19200);
    cfsetospeed(&options, B19200);

    options.c_cflag = B19200 | CS8 | CREAD | CLOCAL | HUPCL;
    options.c_iflag = IGNPAR;
    options.c_lflag = 0;
    options.c_oflag = 0;

    tcsetattr(this->m_fd, TCSANOW, &options);


    printf("fd = %d\n", this->m_fd);
}

int Uart::read_buff(unsigned char *buff, int buff_size) {

    size_t size;
    
    ioctl(this->m_fd, FIONREAD, &size);

    if (size == 0)
	return 0;

  //  if (size > buff_size)
	size = buff_size;

    size = read(this->m_fd, buff, size);
    if (size == -1)
        ROS_ERROR("READ ERROR");

    return size;
}

int Uart::write_buff(unsigned char *buff, int len) {

    if (! len)
	   return 0;

    return write(this->m_fd, buff, len);
    return 0;
}

