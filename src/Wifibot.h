#ifndef WIFIBOT_H
#define WIFIBOT_H

using namespace std;
#include "Uart.h"
#include "wifibot/wifibot_cmd.h"
#include "ros/ros.h"

class Wifibot {

	public:
		Wifibot(Uart& uart, ros::NodeHandle *nh);
		~Wifibot();
		void run(); // launch threads

		void set_speed(unsigned short speed_l, unsigned short speed_r);

		long get_odom_front_left()  { return this->m_odom_front_left;  }
		long get_odom_front_right() { return this->m_odom_front_right; }
		long get_odom_back_left()   { return this->m_odom_back_left;   }
		long get_odom_back_right()  { return this->m_odom_back_right;  }

		double get_temp() { return this->m_temp; }
		double get_hygro() { return this->m_hygro; }
		double get_tension() { return this->m_tension; }
		double get_current() { return this->m_current; }
		double get_capacity() { return this->m_capacity; }

		short get_speed_front_left() { return this->m_speed_front_left; }
		short get_speed_front_right() { return this->m_speed_front_right; }

		float get_ir_front_left() { return this->m_ir_front_left; }
		float get_ir_front_right() { return this->m_ir_front_right; }
		float get_ir_back_left() { return this->m_ir_back_left; }
		float get_ir_back_right() { return this->m_ir_back_right; }

		
		short get_cmd_speed_front_left() { return this->m_cmd_speed_front_left; }
		short get_cmd_speed_front_right() { return this->m_cmd_speed_front_right; }
		short get_cmd_speed_back_left() { return this->m_cmd_speed_back_left; }
		short get_cmd_speed_back_right() { return this->m_cmd_speed_back_right; }

		static short crc16(unsigned char *adresse_tab, unsigned char taille_max);
		static void parse(Wifibot *wifibot, unsigned char *buff);

		void cmd_callback(const wifibot::wifibot_cmd::ConstPtr& msg);

		float range_to_meter(unsigned char range);

	private:

		static void *read_thread(void *arg);
		static void *write_thread(void *arg);

		long m_odom_front_left;
		long m_odom_front_right;
		long m_odom_back_left;
		long m_odom_back_right;

		float m_temp;
		float m_hygro;

		short m_speed_front_left;
		short m_speed_front_right;

		unsigned char m_tension;
		unsigned char m_current;
		unsigned char m_capacity;

		float m_ir_front_left;
		float m_ir_front_right;
		float m_ir_back_left;
		float m_ir_back_right;

		unsigned int wtd;

		Uart* m_uart;


		signed short m_cmd_speed_front_left;
		signed short m_cmd_speed_front_right;
		signed short m_cmd_speed_back_left;
		signed short m_cmd_speed_back_right;

		ros::NodeHandle *nh;

		enum e_state_recep {
			eSTATE_MAGIC_NUMBER = 0,
			eSTATE_LENGTH,
			eSTATE_DATA,
			eSTATE_CRC_L,
			eSTATE_CRC_H
		};
};

#endif
