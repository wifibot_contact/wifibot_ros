#include "Wifibot.h"
#include "pthread.h"
#include "ros/ros.h"
#include "Uart.h"
#include <unistd.h>

using namespace std;

Wifibot::Wifibot(Uart& uart, ros::NodeHandle *nh) {
	this->m_odom_front_left = 0;
	this->m_odom_front_right = 0;
	this->m_odom_back_left = 0;
	this->m_odom_back_right = 0;

	this->m_tension = 0;
	this->m_current = 0;
	this->m_temp = 0;
	this->m_hygro = 0;

	this->m_speed_front_left = 0;
	this->m_speed_front_right = 0;

	this->m_tension = 0;
	this->m_current = 0;
	this->m_capacity = 0;

	this->m_ir_front_left = 0;
	this->m_ir_front_right = 0;
	this->m_ir_back_left = 0;
	this->m_ir_back_right = 0;


	this->m_cmd_speed_front_left = 0;
	this->m_cmd_speed_front_right = 0;
	this->m_cmd_speed_back_left = 0;
	this->m_cmd_speed_back_right = 0;

	this->wtd = 0;

	this->m_uart = &uart;
}

void Wifibot::run() {
	
	int tmp;

	// this->nh->subscribe("wifibot_cmd", 1, &Wifibot::cmd_callback, this);

	pthread_t thread_read, thread_write;

	if ( pthread_create(&thread_read, NULL, Wifibot::read_thread, this)) {
			ROS_INFO("THREAD READ ERROR");
	}


	if ( pthread_create(&thread_write, NULL, Wifibot::write_thread, this)) {
			ROS_INFO("THREAD WRITE ERROR");
	}
}

void * Wifibot::write_thread(void *arg) {

	Wifibot *wifibot;
	wifibot = (Wifibot *) arg;

	unsigned char buff[30];

	short crc;

	while (1) {
		
		buff[0] = 0xFF;
		buff[1] = 12;
		buff[2] = (unsigned char) (abs(wifibot->get_cmd_speed_front_left()) & 0xff);
		buff[3] = (unsigned char) (abs(wifibot->get_cmd_speed_front_left())>>8 & 0xff);
		buff[4] = (unsigned char) (abs(wifibot->get_cmd_speed_front_right()) & 0xff);
		buff[5] = (unsigned char) (abs(wifibot->get_cmd_speed_front_right())>>8 & 0xff);
		buff[6] = (unsigned char) (abs(wifibot->get_cmd_speed_back_left()) & 0xff);
		buff[7] = (unsigned char) (abs(wifibot->get_cmd_speed_back_left())>>8 & 0xff);
		buff[8] = (unsigned char) (abs(wifibot->get_cmd_speed_back_right()) & 0xff);
		buff[9] = (unsigned char) (abs(wifibot->get_cmd_speed_back_right())>>8 & 0xff);
		buff[10] = 
			(((wifibot->get_cmd_speed_front_right() > 0) ? 1 : 0) << 0) | // front right direction
			(((wifibot->get_cmd_speed_front_left()  > 0) ? 1 : 0) << 1) | // front left direction
			(1) << 2; // Enable closed loop
		
		buff[11] = 
			(1) << 0 | // full odometry
			(1) << 1 | // sleep mode
			(0) << 2 | // 5V output
			(0) << 3 | // relay 2
			(0) << 4 | // relay 3
			(0) << 5 | // relay 4
			(((wifibot->get_cmd_speed_back_right() > 0) ? 1 : 0) << 6) | // back right direction
			(((wifibot->get_cmd_speed_back_left() > 0 ) ? 1 : 0) << 7);  // back left direction

		crc = Wifibot::crc16(buff+1, 11);
		buff[12] = (unsigned char) (crc & 0xFF);
		buff[13] = (unsigned char) ((crc>>8) & 0xFF);
		
		wifibot->m_uart->write_buff(buff, 14);

		usleep(20000);
	}
}

void * Wifibot::read_thread(void *arg) {


	Wifibot *wifibot;
	wifibot = (Wifibot *) arg;

	int size;
	unsigned char buff_read;
	unsigned char buff[30];
	int nbuff=0;
	int len_save;

	short crc_r, crc_check;
	int state = 0;
	/*
	 * state:
	 * 0- waiting for...
	 * 1- Start byte OK !
	 */



	e_state_recep state_recep = eSTATE_MAGIC_NUMBER;

	while (1) {

		if (wifibot->m_uart->read_buff(&buff_read, 1) <= 0) {
			usleep(1000);
			continue;
		}

		switch (state_recep) {
	
			case eSTATE_MAGIC_NUMBER:
				if (buff_read == 0xFE) {
					nbuff = 0;
					state_recep = eSTATE_LENGTH;
				}
				break;

			case eSTATE_LENGTH:
				buff[nbuff++] = buff_read;

				if (buff_read != 31)
					state_recep = eSTATE_MAGIC_NUMBER;

				len_save = 31;
				state_recep = eSTATE_DATA;
				break;

			case eSTATE_DATA:
				buff[nbuff++] = buff_read;

				if (nbuff == len_save -1)
					state_recep = eSTATE_CRC_L;
				break;

			case eSTATE_CRC_L:
				crc_r = buff_read & 0x00FF;
				state_recep = eSTATE_CRC_H;
				break;

			case eSTATE_CRC_H:
				crc_r |= buff_read <<8;
				
				crc_check = Wifibot::crc16(buff, (unsigned char) 30);

				if (crc_check == crc_r) {

					Wifibot::parse(wifibot, buff);
				}
				else {
					printf("bad crc !!\n");
				}

				state_recep = eSTATE_MAGIC_NUMBER;

				break;
		}
	}
}

short Wifibot::crc16(unsigned char *adresse_tab , unsigned char taille_max) {

    unsigned int Crc = 0xFFFF;
    unsigned int Polynome = 0xA001;
    unsigned int CptOctet = 0;
    unsigned int CptBit = 0;
    unsigned int Parity= 0;

    Crc = 0xFFFF;
    Polynome = 0xA001; // Polynôme = 2^15 + 2^13 + 2^0 = 0xA001.

    for ( CptOctet= 0 ; CptOctet < taille_max ; CptOctet++)
    {
        Crc ^= *( adresse_tab + CptOctet); //Ou exculsif entre octet message et CRC

        for ( CptBit = 0; CptBit <= 7 ; CptBit++) /* Mise a 0 du compteur nombre de bits */
        {
            Parity= Crc;
            Crc >>= 1; // Décalage a droite du crc
            if (Parity%2 == 1) Crc ^= Polynome; // Test si nombre impair -> Apres decalage à droite il y aura une retenue
        } // "ou exclusif" entre le CRC et le polynome generateur.
    }
    return(Crc);
}

void Wifibot::parse(Wifibot *wifibot, unsigned char *buff) {

	wifibot->m_odom_front_left  = buff[6]  | buff[7]  <<8 | buff[8]  <<16 | buff[9]  <<24;
	wifibot->m_odom_front_right = buff[14] | buff[15] <<8 | buff[16] <<16 | buff[17] <<24;
	wifibot->m_odom_back_left   = buff[18] | buff[19] <<8 | buff[20] <<16 | buff[21] <<24;
	wifibot->m_odom_back_right  = buff[22] | buff[23] <<8 | buff[24] <<16 | buff[25] <<24;
	
	wifibot->m_tension  = buff[3];
	wifibot->m_current  = buff[26];
	wifibot->m_capacity = buff[27];

	wifibot->m_speed_front_left  = buff[1] | buff[2] <<8;
	wifibot->m_speed_front_right = buff[10] | buff[11] <<8;

	wifibot->m_ir_front_left  = wifibot->range_to_meter(buff[4]);
	wifibot->m_ir_front_right = wifibot->range_to_meter(buff[5]);
	wifibot->m_ir_back_left   = wifibot->range_to_meter(buff[12]);
	wifibot->m_ir_back_right  = wifibot->range_to_meter(buff[13]);

	wifibot->m_temp = buff[28] /10;
	wifibot->m_hygro = buff[29] /10;
}

void Wifibot::cmd_callback(const wifibot::wifibot_cmd::ConstPtr& msg) {
	
	this->m_cmd_speed_front_left = msg->speed_front_left;
	this->m_cmd_speed_front_right = msg->speed_front_right;
	this->m_cmd_speed_back_left = msg->speed_back_left;
	this->m_cmd_speed_back_right = msg->speed_back_right;

	// ROS_INFO("CMD OK DIRL=(%d)", this->dir_l);
	// printf("R:%u\n\r", this->speed_r);
	// printf("L:%u\n\r", this->speed_l);
}


float Wifibot::range_to_meter(unsigned char range) {

	float ret = 94.62 / ((float)(range*2) -16.92);

	if (ret > 1.0)
		ret = +1.0;
	else if (ret < 0.2)
		ret = 0.2;

	return ret;
}
