#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>

#include "Uart.h"
#include "Wifibot.h"

#include "wifibot/wifibot_sensors.h"
#include "wifibot/wifibot_cmd.h"

#include <sensor_msgs/Range.h>

int main(int argc, char **argv) {
	ros::init(argc, argv, "wifibot");
	ros::NodeHandle n;

	ros::Publisher wifibot_pub = n.advertise<wifibot::wifibot_sensors>("wifibot_sensors", 1000);
	wifibot::wifibot_sensors wifibot_sensors_msg;
	
	ros::Publisher ir_range_pub = n.advertise<sensor_msgs::Range>("wifibot_ir_range", 1000);
	sensor_msgs::Range ir_range_msg;
	
	ros::Rate loop_rate(15);

	Uart uart("/dev/ttyUSB0", 19200);

	if ( ! uart.is_opened()) {
        	ROS_ERROR("UART NOT OPENED");
		return 1;
	}

	Wifibot *wifibot = new Wifibot(uart, &n);


	wifibot->run(); // start read & write threads

	ros::Subscriber wifibot_cmd = n.subscribe("wifibot_cmd", 1000, &Wifibot::cmd_callback, wifibot);

	unsigned int seq = 0;

	while (ros::ok()) {
		
		wifibot_sensors_msg.odom_front_left = wifibot->get_odom_front_left();
		wifibot_sensors_msg.odom_front_right = wifibot->get_odom_front_right();
		wifibot_sensors_msg.odom_back_left = wifibot->get_odom_back_left();
		wifibot_sensors_msg.odom_back_right = wifibot->get_odom_back_right();

		wifibot_sensors_msg.temp = wifibot->get_temp();
		wifibot_sensors_msg.hygro = wifibot->get_hygro();
		wifibot_sensors_msg.tension= wifibot->get_tension();
		wifibot_sensors_msg.current = wifibot->get_current();
		wifibot_sensors_msg.capacity = wifibot->get_capacity();

		wifibot_sensors_msg.speed_front_left = wifibot->get_speed_front_left();
		wifibot_sensors_msg.speed_front_right = wifibot->get_speed_front_right();
		
		wifibot_pub.publish(wifibot_sensors_msg);

		// IR
		ir_range_msg.radiation_type = sensor_msgs::Range::INFRARED;
		ir_range_msg.min_range = 1.5;
		ir_range_msg.max_range = 0.2;
		ir_range_msg.field_of_view = 0;

		ir_range_msg.header.seq = seq++;
		ir_range_msg.header.stamp = ros::Time::now();
		ir_range_msg.header.frame_id = "front_left";
		ir_range_msg.range = wifibot->get_ir_front_left();
		ir_range_pub.publish(ir_range_msg);
		
		ir_range_msg.header.seq = seq++;
		ir_range_msg.header.stamp= ros::Time::now();
		ir_range_msg.header.frame_id = "front_right";
		ir_range_msg.range = wifibot->get_ir_front_right();
		ir_range_pub.publish(ir_range_msg);
		
		ir_range_msg.header.seq = seq++;
		ir_range_msg.header.stamp = ros::Time::now();
		ir_range_msg.header.frame_id = "back_left";
		ir_range_msg.range = wifibot->get_ir_back_left();
		ir_range_pub.publish(ir_range_msg);
		
		ir_range_msg.header.seq = seq++;
		ir_range_msg.header.stamp = ros::Time::now();
		ir_range_msg.header.frame_id = "back_right";
		ir_range_msg.range = wifibot->get_ir_back_right();
		ir_range_pub.publish(ir_range_msg);

		ros::spinOnce();
		loop_rate.sleep();
	}


	delete &uart;

}
