//
// Created by rbeal on 15/03/17.
//

#ifndef PROJECT_UART_H
#define PROJECT_UART_H

#include <string>

using namespace std;

class Uart {
public:
    Uart();
    ~Uart();
    Uart(string path, int speed);
    int read_buff(unsigned char *buff, int buff_size);
    int write_buff(unsigned char *buff, int len);
    bool is_opened() { return (this->m_fd > 0) ? true : false; }


private:
    int m_baudrate;
    int m_fd;
};


#endif //PROJECT_UART_H
